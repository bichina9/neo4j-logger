package com.maxdemarzi;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.event.LabelEntry;
import org.neo4j.graphdb.event.PropertyEntry;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.helpers.collection.Iterables;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by vladimir on 19.09.17.
 */
public class FormattedTransactionData {
    private Long transactionID;
    private List<Long>
            createdNodes, changedNodes;
    private List<Long> deletedNodes;

    private List<Long> createdRels;
    private List<Long> deletedRels;
    private List<Long> changedRels;

    private List<String> toListString(List<Long> list) {
        return list.stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    private String getJsonStringArray(List<Long> list) {
        return " [ " + String.join("," , toListString(list)) + " ] ";
    }

    FormattedTransactionData(TransactionData transactionData) {
        Iterable<PropertyEntry<Node>> changedNodeProps =
                Iterables.concat(transactionData.assignedNodeProperties(), transactionData.removedNodeProperties());
        Iterable<PropertyEntry<Relationship>> changedRelProps =
                Iterables.concat(transactionData.assignedRelationshipProperties(), transactionData.removedRelationshipProperties());
        transactionID = transactionData.getTransactionId();
        Stream<Long> assignedLabelsNodesStream = StreamSupport
                .stream(transactionData.assignedLabels().spliterator(), false)
                .map(LabelEntry::node)
                .map(Node::getId);
        Stream<Long> unassignedLabelsNodesStream = StreamSupport
                .stream(transactionData.removedLabels().spliterator(), false)
                .map(LabelEntry::node)
                .map(Node::getId);
        Stream<Long> changedNodesStream = Stream.concat(StreamSupport
                        .stream(changedNodeProps.spliterator(), false)
                        .map(PropertyEntry::entity)
                        .map(Node::getId),
                assignedLabelsNodesStream);
        changedNodesStream = Stream.concat(changedNodesStream, unassignedLabelsNodesStream);
        changedNodes = changedNodesStream.distinct().collect(Collectors.toList());

        changedRels = StreamSupport
                .stream(changedRelProps.spliterator(), false)
                .map(PropertyEntry::entity)
                .distinct()
                .map(Relationship::getId)
                .collect(Collectors.toList());

        createdNodes = Iterables.asList(transactionData.createdNodes())
                .stream()
                .map(Node::getId)
                .collect(Collectors.toList());
        deletedNodes = Iterables.asList(transactionData.deletedNodes())
                .stream()
                .map(Node::getId)
                .collect(Collectors.toList());
        createdRels = Iterables.asList(transactionData.createdRelationships())
                .stream()
                .map(Relationship::getId)
                .collect(Collectors.toList());
        deletedRels = Iterables.asList(transactionData.deletedRelationships())
                .stream()
                .map(Relationship::getId)
                .collect(Collectors.toList());
    }

    String toJson() {
        try {
            return "{ \"transactionID\": " + transactionID + ", \"createdNodes\": " + getJsonStringArray(createdNodes)
                    + ", \"deletedNodes\": " + getJsonStringArray(deletedNodes)
                    + ", \"changedNodesStream\": " + getJsonStringArray(changedNodes)
                    + ", \"createdRels\": " + getJsonStringArray(createdRels)
                    + ", \"deletedRels\": " + getJsonStringArray(deletedRels)
                    + ", \"changedRels\": " + getJsonStringArray(changedRels) + " }";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
