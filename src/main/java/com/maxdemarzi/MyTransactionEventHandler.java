package com.maxdemarzi;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.graphdb.event.TransactionEventHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MyTransactionEventHandler implements TransactionEventHandler {

    private static String logfile_path = System.getenv("LOGFILE");

    MyTransactionEventHandler() {}

    @Override
    public Object beforeCommit(TransactionData transactionData) throws Exception {
        return null;
    }

    @Override
    public void afterCommit(TransactionData transactionData, Object o) {
        if (logfile_path == null) {
            System.out.println("LOGFILE is null, skipping");
            return;
        }
        String transactionJson = new FormattedTransactionData(transactionData).toJson();
        FileWriter fw = null;
        BufferedWriter bw = null;
        try {
            File file = new File(logfile_path);
            if (!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(transactionJson);
            bw.newLine();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void afterRollback(TransactionData transactionData, Object o) {

    }
}